import "./Navigation.css";

//Notre composant peut maintenant recevoir des props
function Navigation(props) {
  //On destructure les props en aller chercher le user nécessaire
  const {user} = props

  return (
    <ul className="Navigation">
      <li>
        <img src="https://via.placeholder.com/200x100" alt="Logo" />
      </li>
      <li>
        <h2>Awesome website!</h2>
      </li>
      {/*Opérateur ternaire pour vérifier si l'utilisateur est connecté : 
      affiche son nom si oui, un message si non. On peut utiliser user qu'on est allé chercher dans les props*/}
      <li>{ user ? `${user?.name}` : "Please log in"} </li>
    </ul>
  );
}

export default Navigation;
