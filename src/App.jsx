import { useState } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Profile from "./components/Profile";

function App() {
  const [user, setUser] = useState(null);

  return (
    <div className="App">
      {/*On donne les props aux enfants, à l'intérieur du composant Profile
      des fonctions changent la valeur de user, que l'on donne aussi à Navigation*/}
      <Navigation user={user}/>
      <Profile user={user} setUser={setUser} />
    </div>
  );
}

export default App;
